## Présentation

Les contenaires présentés ici proviennent initialement de :
- https://github.com/liliasfaxi/hadoop-cluster-docker
- https://hub.docker.com/r/rocker/r-base/dockerfile

Le premier est un docker pour simuler un cluster Hadoop et le second est une image de base de R. 

Dans l'image rhadoop, ces deux conténaires sont mis ensemble afin de disposer d'un cluster RHadoop (R+Haddop). Le cluster proposé est un cluster de trois conténaires, avec comme plateformes installées:

  * [Apache Hadoop](http://hadoop.apache.org/) Version: 2.7.2
  * [Apache Spark](https://spark.apache.org/) Version: 2.2.1
  * [Apache Kafka](https://kafka.apache.org/) Version 2.11-1.0.2 
  * [Apache HBase](https://hbase.apache.org/) Version 1.4.8
  * [rbase 4.0.2](https://hub.docker.com/r/rocker/r-base/dockerfile)
  * [rstudio 1.3](https://rstudio.com/products/rstudio/download-server/debian-ubuntu/)
  * [RHadoop (rhdfs, rmr2)](https://github.com/RevolutionAnalytics/RHadoop/wiki)
  * [Purchases](https://drive.google.com/file/d/1Ar12GZISlf-JMQSj1FQsy0_XhSqMH4F9/view)

Pour utiliser ce package, il faudra suivre les étapes suivantes:

1. Installer docker. Pour cela il faudra consulter ce [lien](https://docs.docker.com/docker-for-windows/install/);

2. Télécharger l'image rhadoop 
```
Docker pull gnathan/rhadoop:v0.0.5
```

3. Lancer le cluster

```
git clone ...
cd rhadoop-cluster-docker
./.start-container.sh
```

4. Demarrer Hadoop
```
./start-hadoop.sh
```

5. Se connecter à Rstudio. Lancer dans votre navigateur, la commande suivante:
```
http://localhost:8787/auth-sign-in
```

Les informations pour se connecter sont:
Users: rstudio
Password: rstudio

5. Configurer les variables d'environnement. Avant de charger les librairies `rhdfs` et `rmr2`, il faut définir les variables d'environnement suivant:

```
Sys.setenv(JAVA_HOME="path")
Sys.setenv(HADOOP_HOME="path") 
Sys.setenv(HADOOP_CMD="path")

Sys.setenv(HADOOP_STREAMING="path")

library(rmr2)
library(rhdfs)
#exécution en mode hadoop
rmr.options(backend="hadoop")

```

Bonne lecture!
